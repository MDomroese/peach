#pragma once
#include <string>
#include <iostream>
#include <Windows.h>
#include "Token.h"

enum console_colors {
	BLUE = 1,
	GREEN = 2,
	CYAN = 3,
	RED = 4,
	MAGENTA = 5,
	YELLOW = 6,
	WHITE = 7,
	GREY = 8,
	LIGHTBLUE = 9,
	LIGHTGREEN = 10,
	LIGHTCYAN = 11,
	LIGHTRED = 12,
	LIGHTMAGENTA = 13,
	LIGHTYELLOW = 14,
	LIGHTWHITE = 15
};

void print(std::string s, console_colors color, bool new_line) {
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hCon, color);
	std::cout << s << (new_line ? "\n" : " ");
	SetConsoleTextAttribute(hCon, LIGHTWHITE);
}

void print(std::string s, bool new_line) {
	std::cout << s << (new_line ? "\n" : " ");
}

void print(const char chars[], bool new_line) {
	std::cout << chars << (new_line ? "\n" : " ");
}

void print(const char chars[], console_colors color, bool new_line) {
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hCon, color);
	std::cout << chars << (new_line ? "\n" : " ");
	SetConsoleTextAttribute(hCon, LIGHTWHITE);
}

void print(Token t) {
	print("[ line " + std::to_string(t.line) + " ] [" + to_string(t.token_type) + "]\t[\"" + t.lexeme + "\"]\t[" + t.literal.TypeName() + "]", YELLOW, true);
}

void error() {
	print("[ line ? ] Error: Unhandled.", RED, true);
}

void error(int line) {
	print("[ line " + std::to_string(line) + " ] Error: Unhandled.", RED, true);
}

void error(int line, std::string message) {
	print("[ line " + std::to_string(line) + " ] Error: " + message + ".", RED, true);
}

void error(int line, int ccount, std::string message) {
	print("[ line " + std::to_string(line) + " ] Error at position " + std::to_string(ccount) + ": " + message + ".", RED, true);
}
