#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "Console.h"
#include "Scanner.h"

const std::string VERSIONNR = "0.0.2.3";
const std::string EXTENSION = ".peach";


void proc_command_line(std::string line);
void proc_prompt(std::string com);
void proc_instruction(std::string ins);
void proc_file(std::string path);
void repl();


static Scanner scanner = Scanner();


int main(int argc, char* argv[]) {
	if (argc != 2) {
		repl();
	}
	else {
		std::vector<std::string> args(argv + 1, argv + argc);
		proc_file(args[0]);
	}

	return 0;
}

void repl() {
	std::string input;
	while (input != "?q") {
		print("\nPeach~ ", CYAN, false);
		std::getline(std::cin, input);
		proc_command_line(input);
	}
}

void proc_file(std::string path) {
	if (path.find(EXTENSION) == std::string::npos) {
		print("Wrong file extension.\nUsage: ?c *.peach", RED, true);
		return;
	}

	std::ifstream f(path.c_str());
	
	if (!f.good()) {
		print("File doesn't exist.\nPlease check the path.", RED, true);
		return;
	}

	std::string l;
	scanner.Init();
	StackTrace st = scanner.stack_trace;

	while (getline(f, l) && !st.error_occurred())
		scanner.ReadLine(l);

	f.close();

	if (st.error_occurred()) {
		error(st.loc, st.ccount, st.msg);
		return;
	}

	/*for (Token t : scanner.Dump())
		print(t);*/
}

void proc_command_line(std::string line) {
	if (line.empty())
		return;
	if (line.rfind('?', 0))
		proc_prompt(line);
	else
		proc_instruction(line);
}

void proc_prompt(std::string line) {
	scanner.Init();
	scanner.ReadLine(line);

	if (scanner.stack_trace.error_occurred()) {
		error(scanner.stack_trace.loc, scanner.stack_trace.ccount, scanner.stack_trace.msg);
		return;
	}

	/*for (Token t : scanner.Dump())
		print(t);*/
}

void proc_instruction(std::string ins) {
	int c = ins[1];

	switch (c)
	{
	case 'i':
		print("Peach Compiler v." + VERSIONNR + "\n_______________________\nExit:\t\t'?q'\nHelp:\t\t'?h'", YELLOW, true);
		break;
	case 'q':
		print("Exiting.", GREY, true);
		break;
	case 'h':
		print("Nobody is there to help. :<", YELLOW, true);
		break;
	case 'c':
		proc_file(ins.substr(3));
		break;
	default:
		print("Unknown command.", YELLOW, true);
		break;
	}
}