#pragma once
#include <string>
#include <vector>
#include "Token.h"

// Container class for possible error stack trace.
class StackTrace {
public:
	int loc;
	int ccount;
	std::string msg;

	StackTrace(int line, int char_count, std::string message) {
		loc = line;
		ccount = char_count;
		msg = message;
	}

	StackTrace() {
		loc = -1;
		ccount = 0;
		msg = "";
	}

	bool error_occurred() {
		return loc != -1;
	}
};

// Class for Lexical Analysis.
class Scanner {
public:
	StackTrace stack_trace;

	void Init() {
		tokens.clear();
		loc = 1;
		current = 0;
		start = 0;
		stack_trace = StackTrace();
	}

	void ReadLine(std::string l) {
		line = l;
		current = 0;
		start = 0;
		while (!at_end() && !stack_trace.error_occurred())
			scan();

		add_token(TokType::LINEBREAK);
		loc++;
	}

	std::vector<Token> Dump() {
		return tokens;
	}

private:
	std::vector<Token> tokens;
	std::string line;
	int loc;
	int current;
	int start;

	bool at_end() {
		return current >= line.length();
	}

	void scan() {
		// Check Space
		if (is_space(peek())) {
			start = ++current;
			return;
		}

		char c = advance();

		// Check Comment
		if (c == '#') {
			while (!at_end()) advance();
			return;
		}

		// Check string literal
		if (c == '"') {
			while (!at_end() && peek() != '\"')
				advance();

			if (peek() != '\"')
				stack_trace = StackTrace(loc, current, "Missing enclosing '\"' for string literal");
			else
				advance();

			add_token(TokType::STRING, String(line.substr(start + 1, current - 1)));
			return;
		}

		// Check Number Literal
		if (is_number(c)) {
			while (is_number(peek())) advance();

			if (peek() == '.') {
				advance();

				if (!is_number(peek())) {
					stack_trace = StackTrace(loc, current, "No trailing commas allowed for floats on either side");
				}
				else {
					while (is_number(peek())) advance();
					add_token(TokType::FLOAT, Float(std::stof(lexeme())));
				}
			}
			else
				add_token(TokType::INT, Integer(std::stoi(lexeme())));
		}

		// Check Non-Lookahead Single Character Tokens
		switch (c) {
		case '(': add_token(TokType::LEFT_PAREN); break;
		case ')': add_token(TokType::RIGHT_PAREN); break;
		case '{': add_token(TokType::LEFT_BRACE); break;
		case '}': add_token(TokType::RIGHT_BRACE); break;
		case ',': add_token(TokType::COMMA); break;
		case '.': add_token(TokType::DOT); break;
		case '^':add_token(TokType::CARET); break;
		case '*': add_token(TokType::STAR); break;
		case '?': add_token(TokType::QMARK); break;
		}

		// Check Single-Lookahead Multicharacter Tokens
		switch (c)
		{
		case '=': add_token(peek_match('=') ? TokType::EQEQ : TokType::EQ); break;
		case '>': add_token(peek_match('=') ? TokType::GREATER_EQ : TokType::GREATER); break;
		case '<': add_token(peek_match('=') ? TokType::SMALLER_EQ : TokType::SMALLER); break;
		case '!': add_token(peek_match('=') ? TokType::NEQ : TokType::NOT); break;
		case '&': add_token(peek_match('&') ? TokType::AND : TokType::BIT_AND); break;
		case '|': add_token(peek_match('|') ? TokType::OR : TokType::BIT_OR); break;
		case '-': add_token(peek_match('-') ? TokType::DEC : TokType::MINUS); break;
		case '+': add_token(peek_match('+') ? TokType::INC : TokType::PLUS); break;
		}

		// Check Multi-Lookahead Multicharacter Tokens
		if (is_alphabetic(c)) {
			while (is_alphanumeric(peek())) advance();
			// Check if literal is keyword
			auto f = keywords.find(lexeme());
			if (f != keywords.end())
				add_token(f->second);
			else
				add_token(TokType::IDENTIFIER);
		}
	}

	// Gives the current lexeme.
	std::string lexeme() {
		return line.substr(start, current - start);
	}

	// Returns and consumes next character.
	char advance() {
		return line[current++];
	}

	// Returns next character without consuming.
	char peek() {
		return at_end() ? '\0' : line[current];
	}

	bool is_space(char c) {
		return c == '\t' || c == ' ' || c == '\r';
	}

	// Peeks character and consumes, if it matches the input.
	bool peek_match(char c) {
		if (at_end())
			return false;

		if (line[current] == c) {
			current++;
			return true;
		}

		return false;
	}

	// Peeks string and consumes, if it matches the input. 
	bool peek_match(std::string s) {
		if (at_end() || current + s.length() > line.length())
			return false;

		if (line.substr(current, current + s.length()) == s)
			for (char c : s)
				advance();

		return true;
	}

	// Check if char is a numeric value.
	bool is_number(char c) {
		return c >= '0' && c <= '9';
	}

	bool is_alphabetic(char c) {
		return (c >= 'a' && c <= 'z')
			|| (c >= 'A' && c <= 'Z')
			|| (c == '_');
	}

	bool is_alphanumeric(char c) {
		return (c >= 'a' && c <= 'z')
			|| (c >= 'A' && c <= 'Z')
			|| (c >= '0' && c <= '9')
			|| (c == '_');
	}

	// Adds a token to scanner list.
	void add_token(TokType t) {
		add_token(t, Object());
	}

	// Adds a token to scanner list.
	void add_token(TokType t, Object literal) {
		tokens.push_back(Token(t, literal, lexeme(), loc));
		start = current;
	}
};
