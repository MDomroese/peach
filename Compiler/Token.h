#pragma once
#include <string>
#include <unordered_map>

enum class TokType {
	// Single Character
	LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, COMMA, DOT, MINUS, PLUS,
	LINEBREAK, SLASH, STAR, HASH, CARET, QMARK, NOT,

	// Multi Character
	EQ, NEQ, EQEQ, GREATER, GREATER_EQ, SMALLER, SMALLER_EQ, OR, AND, BIT_AND, BIT_OR,
	INC, DEC,

	// Literals
	IDENTIFIER, STRING, FLOAT, INT,

	// Keywords
	VAR, CLASS, IF, ELSE, WHILE, FOR, _FALSE, _TRUE, FUNC, _NULL, PRINT, RETURN, BASE,
	_THIS,

	NONE
};

// Let's hope C++20 releases soon...
std::string to_string(TokType ttype) {
	switch (ttype)
	{
	case TokType::LEFT_PAREN: return "L_PAREN";
	case TokType::RIGHT_PAREN: return "R_PAREN";
	case TokType::LEFT_BRACE: return "L_BRACE";
	case TokType::RIGHT_BRACE: return "R_BRACE";
	case TokType::COMMA: return "COMMA";
	case TokType::DOT: return "DOT";
	case TokType::MINUS: return "MINUS";
	case TokType::PLUS: return "PLUS";
	case TokType::LINEBREAK: return "BREAK";
	case TokType::SLASH: return "SLASH";
	case TokType::STAR: return "STAR";
	case TokType::HASH: return "HASH";
	case TokType::CARET: return "CARET";
	case TokType::EQ: return "EQ";
	case TokType::NEQ: return "NEQ";
	case TokType::EQEQ: return "EQEQ";
	case TokType::GREATER: return "GREATER";
	case TokType::GREATER_EQ: return "GR_EQ";
	case TokType::SMALLER: return "SMALLER";
	case TokType::SMALLER_EQ: return "SM_EQ";
	case TokType::OR: return "OR";
	case TokType::AND: return "AND";
	case TokType::IDENTIFIER: return "IDENT";
	case TokType::STRING: return "STRING";
	case TokType::FLOAT: return "FLOAT";
	case TokType::INT: return "INT";
	case TokType::VAR: return "VAR";
	case TokType::CLASS: return "CLASS";
	case TokType::IF: return "IF";
	case TokType::ELSE: return "ELSE";
	case TokType::WHILE: return "WHILE";
	case TokType::FOR: return "FOR";
	case TokType::_FALSE: return "FALSE";
	case TokType::_TRUE: return "TRUE";
	case TokType::FUNC: return "FUNC";
	case TokType::_NULL: return "NULL";
	case TokType::PRINT: return "PRINT";
	case TokType::RETURN: return "RETURN";
	case TokType::BASE: return "BASE";
	case TokType::_THIS: return "THIS";
	case TokType::NONE: return "NONE";
	case TokType::NOT: return "NOT";
	case TokType::BIT_AND: return "BIT_AND";
	case TokType::BIT_OR: return "BIT_OR";
	case TokType::INC: return "INC";
	case TokType::DEC: return "DEC";
	default: return "REEEEEEEEEEEEEEEEEEEE";
	}
}

std::unordered_map<std::string, TokType> keywords = {
	{"var", TokType::VAR},
	{"class", TokType::CLASS},
	{"if", TokType::IF},
	{"else", TokType::ELSE},
	{"while", TokType::WHILE},
	{"for", TokType::FOR},
	{"false", TokType::_FALSE},
	{"true", TokType::_TRUE},
	{"func", TokType::FUNC},
	{"null", TokType::_NULL},
	{"print", TokType::PRINT},
	{"return", TokType::RETURN},
	{"base", TokType::BASE},
	{"this", TokType::_THIS},
};

enum class ObjectType {
	NULL_,
	STRING,
	FLOAT,
	INT
};

class Object {
public:
	ObjectType Type = ObjectType::NULL_;
	virtual std::string TypeName() { return "Object"; }
};

class String : public Object {
public:
	std::string str;
	String(std::string string) {
		Type = ObjectType::STRING;
		str = string;
	}
	std::string TypeName() override { return "String"; }
};

class Float : public Object {
public:
	float fl;
	Float(float f) {
		fl = f;
		Type = ObjectType::FLOAT;
	}
	std::string TypeName() override { return "Float"; }
};

class Integer : public Object {
public:
	int in;
	Integer(int i) {
		in = i;
		Type = ObjectType::INT;
	}
	std::string TypeName() override { return "Integer"; }
};

class Token {
public:
	TokType token_type;
	Object literal;
	std::string lexeme;
	int line;

	Token(TokType type, Object lit, std::string lex, int loc) {
		token_type = type;
		literal = lit;
		lexeme = lex;
		line = loc;
	}
};